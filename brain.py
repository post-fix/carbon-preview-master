import speech_recognition as sr
import os
from time import sleep
import re
from io import StringIO

"""

Project: Carbon Automation AI

Author: Alexandre Trottier

Version: 0.0.3-alpha

Status: Pre-Alpha Design


Project Notes:
	- Currently in the design phase


Modules Currently Implemented:
	-[x] Date
	-[ ] Year
	-[ ] top
To Add modules:
	- Store module written in python in the modules directory
	- Import module into python
	- Add module name (without file extension) into the modules list

To Do:
- [ ] Figure out a way to dynamically execute modules
- [ ] Figure out a way to run the recognize_audio function, and then store the output in a variable to prevent having to
        call the recognize_audio function each time I was to operate on text
- [ ] Implement classes
"""

client_id = 'IDvCAC3BVA7OT6wpF-8Ugw=='
user_id = 'UZ88zlplTdngMb4gvEJBnlp6VAIzrOZ_nQ7KgqV-b4nQtqr6QfZluankTTbpRLIwxU5FQSI95gh_eTNAngOv0g=='

modules = ['date', 'year','top'] # This list contains the modules used by carbon

r = sr.Recognizer()  # thi allows you to make use of the various recognize() methods by accessing it through the variable r


def string_memory_write(text):
    output=io.StringIO()
    r=output.write(text)
    return r

def brain(source, client_id, user_id):
    while True:
        """
        This is the brain function which is the 'brain' of carbon, responsible for calling other functions as needed
        and executing instructions
        """

        text = recognize_audio(client_id, user_id,source)  # runs audio recognize functioning, returning text version of audio
        preprogrammed_response(text)


class recognize_audio(object):
    def __init__(self, client_id, user_id, source):
        """
        Initializes the variables that are going to be used in STT translation
        """
        self.source = source
        self.client_id = client_id
        self.user_id = user_id

    def speech_to_text(self, client_id, user_id, source):
        r = sr.Recognizer()
        audio = r.listen(source)  # listens to the mic
        text = r.recognize_houndify(audio, self.client_id, self.user_id)
        return text


class preprogrammed_response(object):
    def __init__(self, client_id, user_id, source):

        """
        Initializes response words, and declares microphone source
        """

        response_words_list = []  # initializes empty response word list

        self.source = source
        self.client_id = client_id
        self.user_id = user_id
        self.response_words_file = "persistent-files/response_words.txt"  # specifies the file that contains response_words preserved across aplication restarts
        self.response_words_list = response_words_list
        """
####### The section below this doesnt appear to read file line by line and append the data to the response_words_list list it outputs:
####### 'good\ngreat\namazing\nawesome\nbrilliant\nfantastic\nwell'
        """
        #with open(self.response_words_file,'r') as file_handler:  # Opens the response_words_file with fh of 'file_handler' in read only mode, which ensures that the file is closed after being read without having to manualyl close it
        #    line = file_handler.read()  # read file line by line
        #    line = line.strip("\n")  # strips new line character
        #    self.response_words_list.append(line)  # adds the 'response word' to the list

        for line in open(self.response_words_file, 'r'):
            li_ne = line.strip("\n")
            self.response_words_list.append(li_ne)
#
    def carbon_response(self, client_id, user_id, source):

        r = recognize_audio(client_id, user_id, source)  # initializes the recognize_audio class

        while True:  # Stays true until user says 'hello carbon'

            text = r.speech_to_text(client_id, user_id, source)
            if re.search('hello carbon', text):  # If user says 'hello carbon' execute the following block
                break  # breaks the loop
            else:
                print('not a valid response please try again')

        os.system("espeak 'Greetings overlord, how are you today'")
        text = r.speech_to_text(client_id, user_id, source)
        output = StringIO() #Initializes the StringIO() module, which is used to write data to memory
        output.write(text) #Takes the data contained in 'text' and writes it to memory
        print(output.getvalue()) #Retrieves the data stored in memory and prints it
        text=output.getvalue()
        yes_match = {}  # Initializes empty, this will contain the index number and the word from response_words_list if a match is found
        print("\n")
        print("We are now going to parse through 'text' data to see if any known response words are there")
        print("If none are known, then we will extract it and add it to the list of known resposne words")
        print("This searching process can take sometime")
        print("\n")
        for index, word in enumerate(self.response_words_list):# Enumerate returns index of element and the element itself
            if re.search(word, text):  # checks to see if the word is present in the translated text from user
                yes_match[index] = word  # If the word is there, update yes_match{}
                break  # A match was detected so no need to do anymore processing
        if len(yes_match) == 0:  # checks to see if yes_match is empty, if so indicates that no previously known response word is used and we must extract the new one and add it to our list
            """
            This block executes if no known response words were given to carbon, and will ensure that the new word is added to the list of known response words
            """
            if re.search('(?<=doing) (\w+)',text):  # positive lookbehind assertion, if match is true (doing is present) will grab the word after it

                response_word_new = re.search('(?<=doing) (\w+)', text)  # positive look behind assertion, if 'doing' is matched the word after it is grabbed
                response_word_new = response_word_new.group().strip(" ' ")
                print('New response word detected adding to known list of words')
                print('New response word is %s' % response_word_new)
                self.response_words_list.append(
                    response_word_new)  # appends the newly learned word to the response_words_list object
                # Opens the response_words_file, and appends the newly response word to save across reboots
                with open(self.response_words_file,
                          'a') as file_handler:
                    file_handler.write('%s' % response_word_new)

                # cleans up, and closes the memory buffer
                output.close()
                # Resets data in text to nothing
                text = ''

                """
                At this point the newly detected response word has been processed and categorized, we can continue with our program
                """

                os.system(
                    "espeak 'That is excellent melord, what would you like to do'")  # Carbon responds to the user saying how they are today

                text = r.recognize_audio(client_id, user_id, source)
                if re.search('module', text):
                    m = modules
                    m.execution()


                """
                Now that the unknown word was added to the list of known words, begin module execution
                """
        elif len(yes_match) >= 1:
            """
            User has responded to carbon's 'how are you today' question
            """

            os.system("espeak 'That is excellent melord, what would you like to do'")
            text = r.recognize_audio(client_id, user_id, source)
            if re.search('module', text):
                m = modules
                m.execution()

            """
            This block will be execute if previously known words were supplied to carbon, and should call the module_execution portion of the code
            """
            pass

        """
        a=re.search("(?<=doing) (\w+)", phrase)
        This will match a single word after the word 'doing'
        The above is called a 'positive lookbehind assertion see the following link for more information:
            https://docs.python.org/2/library/re.html
            Search for 'lookbehind'
        """


class modules(object):  # This class uses inheritance to inherit class variables from recognize_audio

    """
    Import modules:

    import modules # imports the modules directory
    import importlib # imports importlib library
    importlib.import_module('modules.date') # imports date module


    import imp is needed
    """

    def __init__(self,  client_id, user_id, source):
        self.client_id = client_id
        self.user_id = user_id
        self.source = source

    def execution(self, known_modules):
        os.system("espeak 'What module would you like to run'")
        r = recognize_audio(self.client_id, self.user_id, self.source)
        text = r.speech_to_text(self.client_id, self.user_id, self.source)

        for module in known_modules:

            if re.search(module, text):
                module_name = module
                # Defines the filepath variable which contains the absolute path to the module
                filepath = '/home/pi/personal/carbon-home-automation-ai/modules/%s.py' % module_name
                # os.path.split(filepath) splits the filepath into two parts, one the file being referenced
                # and two the path to the file being referenced
                # os.path.splitext() which splits the string into two objects, (root, ext) in which ext begins w/ a
                # period
                mod_name, file_ext = os.path.splitext(os.path.split(filepath)[-1])
                py_mod = imp.load_source(mod_name, filepath)
                x = py_mod.Module()
                x.execute()

    def information(self):
        pass

print("""\n
	  =====================================================================
	  Carbon Home Automation AI                  Author: Alexandre Trottier
	  =====================================================================
	  
			This is the Carbon Home Automation AI
			Currently in developmental stages
	  \n""")

# sleep(50)


with sr.Microphone() as source:
    x = preprogrammed_response(client_id,user_id,source)
    x.carbon_response(client_id, user_id,source)
