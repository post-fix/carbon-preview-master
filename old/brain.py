import speech_recognition as sr
import os
from time import sleep
import re
"""

Project: Carbon Automation AI

Author: Alexandre Trottier

Version: 0.0.2-alpha

Status: Pre-Alpha Design


Project Notes:
	- Currently in the design phase


Modules Currently Implemented:
	-[x] Date
	-[ ] Year
	-[ ] top
To Add modules:
	- Store module written in python in the modules directory
	- Import module into python
	- Add module name (without file extension) into the modules list
	
	
To Do:
	- [ ] Figure out a way to dynamically execute modules
	- [ ] Figure out a way to run the recognize_audio function, and then store the output in a variable to prevent having to call the recognize_audio function each time I was to operate on text
	- [ ] Implement classes
"""


client_id='IDvCAC3BVA7OT6wpF-8Ugw=='
user_id='UZ88zlplTdngMb4gvEJBnlp6VAIzrOZ_nQ7KgqV-b4nQtqr6QfZluankTTbpRLIwxU5FQSI95gh_eTNAngOv0g=='

modules=['date','year','top'] #This list contains the modules used by carbon, which are specific actions carbon can take. 

r=sr.Recognizer()#thi allows you to make use of the various recognize() methods by accessing it through the variable r

def recognize_audio(client_id, user_id, source):#this function is used to capture audio from the user and process it, returning the text version of audio data

	print('\nRECORDING AUDIO\n')
	audio=r.listen(source)
	text=r.recognize_houndify(audio, client_id, user_id)
	return text






def preprogrammed_response(text):#this function is used for preprogrammed operations with carbon, such as greetings, small talk, etc..

	if re.search('hello carbon',text):

		os.system("espeak 'Greetings overlord, how are you today'")
		text=recognize_audio(client_id, user_id, source)
		
		response_words='good|great|amazing|awesome|brilliant|fantastic|well'#contains a list of possible responses to carbons 'How are you today' question allowing a more natural feeling conversation

		"""
		Need to figure out a way to add more response words either statically, or dynamically
		"""

		if re.search(response_words, text): #Uses the words listed in response_words to match against my response

			os.system("espeak 'That is excellent my lord'")
			os.system("espeak 'What would you like to do'")
			text=recognize_audio(client_id, user_id, source)
			
			"""
			Need to figure out a way to efficiently add more possible responses to 'what would you like to do' question
			"""

			if re.search('module', text):#checks to see if the word module was indicated by the user, if so then begin searching for what modules the user wants

				module_execution()#Executes the module_execution function

	elif re.search('carbon', text):
	
		os.system("espeak 'yes my lord?'")
		text=recognize_audio(client_id, user_id, source)
	
		if re.search('module', text):
			module_execution()
	else:

		os.system("espeak 'I did not understand what you said melord'") #a catch all in case no valid response was given
		print("Carbon thought you said", text)



	
def module_execution():

	os.system("espeak 'What module would you like to run'")
	text=recognize_audio(client_id, user_id, source)

	for module_name in modules:#iterates over each of the possible modules

		if re.search(module_name, text): #Checks to see if the current module name matches that of the module specified by the user

			print(text)
			module_to_execute='%s.py' % module_name#If there was a match then begin execution of module
			current_directory=os.getcwd()
			os.system('%s/%s' % (current_directory, module_to_execute)) #executes the module

		else:#Ensures that if there was no mention of the word module by the user nothing happens

			print("Module name not valid, continuing to listen")
			


def brain(source, client_id, user_id): 
	while True:
	#This is the brain function which is the 'brain' of carbon, responsible for calling other functions as needed and executing instructions
		text=recognize_audio(client_id, user_id,source) #runs audio recognize functioning, returning text version of audio
		preprogrammed_response(text)


			
print("""\n
	  =====================================================================
	  Carbon Home Automation AI                  Author: Alexandre Trottier
	  =====================================================================
	  
			This is the Carbon Home Automation AI
			Currently in developmental stages
	  \n""")

#sleep(50)
with sr.Microphone() as source:
	brain(source, client_id, user_id)
