import datetime
import re
import pyowm

api_key = 'ec64729a33987c7493bae7112e552ce5'
owm = pyowm.OWM(api_key)
city = 'Vancouver'
country_code = 'CAN'
loc = '%s,%s' % (city, country_code)

fc = owm.three_hours_forecast(loc)

forecast = fc.get_forecast()

forecast_list = forecast.get_weathers()

forecast_list_curated = []

now = datetime.datetime.now()

cur_date = '%s-%s-%s' % (now.year, now.month, now.day)
cur_date = '.*%s.*' % cur_date

for weather in forecast_list:

    if re.search(cur_date, str(weather)):
	
        forecast_list_curated.append(weather)

list_of_weather_status = []		
for weather in forecast_list_curated:
	status_info = str(re.search('(status=.*)', str(weather)).groups())
	junk , status = status_info.split('=')
	status = status[:-4]
	list_of_weather_status.append(status)

num_of_entries = len(list_of_weather_status)
rain_count = list_of_weather_status.count('Rain')
if rain_count >= num_of_entries / 2:
	overall_rain = True

print(overall_rain)
