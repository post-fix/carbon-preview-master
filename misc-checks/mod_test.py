import os
import imp


module_name='date'

filepath='/home/pi/personal/carbon-home-automation-ai/modules/%s.py' % module_name

mod_name, file_ext = os.path.splitext(os.path.split(filepath)[-1])

py_mod = imp.load_source(mod_name, filepath)

x = py_mod.Module()
x.test()

