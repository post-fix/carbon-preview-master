import os
import imp
import datetime
from io import StringIO
import re
def brain(source, client_id, user_id):
    while True:
        """
        This is the brain function which is the 'brain' of carbon, responsible for calling other functions as needed
        and executing instructions
        """

        text = recognize_audio(client_id, user_id,source)  # runs audio recognize functioning, returning text version of audio
        preprogrammed_response(text)


class recognize_audio(object):
    def __init__(self, client_id, user_id, source):
        """
        Initializes the variables that are going to be used in STT translation
        """
        self.source = source
        self.client_id = client_id
        self.user_id = user_id

    def speech_to_text(self, client_id, user_id, source):
        r = sr.Recognizer()
        audio = r.listen(source)  # listens to the mic
        text = r.recognize_houndify(audio, self.client_id, self.user_id)
        return text


class preprogrammed_response(object):
    def __init__(self, client_id, user_id, source):

        """
        Initializes response words, and declares microphone source
        """

        response_words_list = []  # initializes empty response word list

        self.source = source
        self.client_id = client_id
        self.user_id = user_id
        self.response_words_file = "persistent-files/response_words.txt"  # specifies the file that contains response_words preserved across aplication restarts
        self.response_words_list = response_words_list
        """
####### The section below this doesnt appear to read file line by line and append the data to the response_words_list list it outputs:
####### 'good\ngreat\namazing\nawesome\nbrilliant\nfantastic\nwell'
        """
        #with open(self.response_words_file,'r') as file_handler:  # Opens the response_words_file with fh of 'file_handler' in read only mode, which ensures that the file is closed after being read without having to manualyl close it
        #    line = file_handler.read()  # read file line by line
        #    line = line.strip("\n")  # strips new line character
        #    self.response_words_list.append(line)  # adds the 'response word' to the list

        for line in open(self.response_words_file, 'r'):
            li_ne = line.strip("\n")
            self.response_words_list.append(li_ne)
#
    def carbon_response(self, client_id, user_id, source):

        r = recognize_audio(client_id, user_id, source)  # initializes the recognize_audio class

        while True:  # Stays true until user says 'hello carbon'

            text = r.speech_to_text(client_id, user_id, source)
            if re.search('hello carbon', text):  # If user says 'hello carbon' execute the following block
                break  # breaks the loop
            else:
                print('not a valid response please try again')

        os.system("espeak 'Greetings overlord, how are you today'")
        text = r.speech_to_text(client_id, user_id, source)
        output = StringIO() #Initializes the StringIO() module, which is used to write data to memory
        output.write(text) #Takes the data contained in 'text' and writes it to memory
        print(output.getvalue()) #Retrieves the data stored in memory and prints it
        text=output.getvalue()
        yes_match = {}  # Initializes empty, this will contain the index number and the word from response_words_list if a match is found
        print("\n")
        print("We are now going to parse through 'text' data to see if any known response words are there")
        print("If none are known, then we will extract it and add it to the list of known resposne words")
        print("This searching process can take sometime")
        print("\n")
        for index, word in enumerate(self.response_words_list):# Enumerate returns index of element and the element itself
            if re.search(word, text):  # checks to see if the word is present in the translated text from user
                yes_match[index] = word  # If the word is there, update yes_match{}
                break  # A match was detected so no need to do anymore processing
        if len(yes_match) == 0:  # checks to see if yes_match is empty, if so indicates that no previously known response word is used and we must extract the new one and add it to our list
            """
            This block executes if no known response words were given to carbon, and will ensure that the new word is added to the list of known response words
            """
            if re.search('(?<=doing) (\w+)',text):  # positive lookbehind assertion, if match is true (doing is present) will grab the word after it

                response_word_new = re.search('(?<=doing) (\w+)', text)  # positive look behind assertion, if 'doing' is matched the word after it is grabbed
                response_word_new = response_word_new.group().strip(" ' ")
                print('New response word detected adding to known list of words')
                print('New response word is %s' % response_word_new)
                self.response_words_list.append(
                    response_word_new)  # appends the newly learned word to the response_words_list object
                # Opens the response_words_file, and appends the newly response word to save across reboots
                with open(self.response_words_file,
                          'a') as file_handler:
                    file_handler.write('%s' % response_word_new)

                # cleans up, and closes the memory buffer
                output.close()
                # Resets data in text to nothing
                text = ''

                """
                At this point the newly detected response word has been processed and categorized, we can continue with our program
                """

                os.system(
                    "espeak 'That is excellent melord, what would you like to do'")  # Carbon responds to the user saying how they are today

                text = r.recognize_audio(client_id, user_id, source)
                if re.search('module', text):
                    m = modules
                    m.execution()


                """
                Now that the unknown word was added to the list of known words, begin module execution
                """
        elif len(yes_match) >= 1:
            """
            User has responded to carbon's 'how are you today' question
            """

            os.system("espeak 'That is excellent melord, what would you like to do'")
            text = r.recognize_audio(client_id, user_id, source)
            if re.search('module', text):
                m = modules
                m.execution()

            """
            This block will be execute if previously known words were supplied to carbon, and should call the module_execution portion of the code
            """
            pass

        """
        a=re.search("(?<=doing) (\w+)", phrase)
        This will match a single word after the word 'doing'
        The above is called a 'positive lookbehind assertion see the following link for more information:
            https://docs.python.org/2/library/re.html
            Search for 'lookbehind'
        """


class modules(object):  # This class uses inheritance to inherit class variables from recognize_audio

    """
    Import modules:

    import modules # imports the modules directory
    import importlib # imports importlib library
    importlib.import_module('modules.date') # imports date module


    import imp is needed
    """

    def __init__(self,  client_id, user_id, source):
        self.client_id = client_id
        self.user_id = user_id
        self.source = source

    def execution(self, known_modules):
        os.system("espeak 'What module would you like to run'")
        r = recognize_audio(self.client_id, self.user_id, self.source)
        text = r.speech_to_text(self.client_id, self.user_id, self.source)

        for module in known_modules:

            if re.search(module, text):
                module_name = module
                # Defines the filepath variable which contains the absolute path to the module
                filepath = '/home/pi/personal/carbon-home-automation-ai/modules/%s.py' % module_name
                # os.path.split(filepath) splits the filepath into two parts, one the file being referenced
                # and two the path to the file being referenced
                # os.path.splitext() which splits the string into two objects, (root, ext) in which ext begins w/ a
                # period
                mod_name, file_ext = os.path.splitext(os.path.split(filepath)[-1])
                py_mod = imp.load_source(mod_name, filepath)
                x = py_mod.Module()
                x.execute()

    def information(self):
        pass


class weather(object):

    def __init__(self, client_id, user_id, source, api_key, country_code, city):
        r = recognize_audio
        self.client_id = client_id
        self.user_id = user_id
        self.source = source
        self.api_key = api_key
        self.country_code = country_code
        self.city = city

    def current_temperature(self, api_key, country_code, city):
        owm = pyowm.OWM(api_key)
        # location
        loc = '%s,%s' % (city, country_code)
        # allows us to interact with the 'observation' object
        observation = owm.weather_at_place(loc)
        # allows us to retrieve weather information from the observation object
        w = observation.get_weather()
        # grabs current temp, which returns a dictionary containing current temp, min temp, and max temp
        temp = w.get_temperature('celsius')
        # grabs the current temperature
        cur_temp = float(temp['temp'])
        os.system("espeak 'It is currently %f degree celsius'" % cur_temp)

    def today_forecast(self,api_key, country_code, city):
        owm = pyowm.OWM(api_key)
        loc = '%s,%s' % (city, country_code)
        fc = owm.three_hours_forecast(loc)
        pass

    def clothing_prediction(self, client_id, user_id, source, api_key, country_code, city):
        # creates object allowing us to interact with openweathermap api
        owm = pyowm.OWM(api_key)
        # sets the location according to ISO 3166 standards
        loc = '%s,%s' % (city, country_code)
        # allows us to interact with fc object which is used to retrieve information about forecast in 3h interval
        fc = owm.three_hours_forecast(loc)
        # retrieves forecast data
        forecast = fc.get_forecast()
        # gets weather information from forecast data in list format
        forecast_list = forecast.get_weathers()
        forecast_list_curated = []
        # sets current day
        now = datetime.datetime.now()
        cur_date = '%s-%s-%s' % (now.year, now.month, now.day)
        cur_date = '.*%s.*' % cur_date
        # iterates over the forecast_list and pulls the information relevant to today
        for weather in forecast_list:
            if re.search(cur_date, str(weather)):
                forecast_list_curated.append(weather)
        # creates an empty list which will be used to contain all the status from the forecast (rain, etc...)
        list_of_weather_status = []
        # iterates over the forecast
        for weather in forecast_list_curated:
            # extracts the status information
            status_info = str(re.search('(status=.*)', str(weather)).groups())
            # isolates status information to 'status' variable
            junk , status = status_info.split('=')
            # strips the last 4 characters off the end
            # removes unneeded characters
            status = status[:-4]
            # appends the status information to the status list
            list_of_weather_status.append(status)

        num_of_entries = len(list_of_weather_status)
        rain_count = list_of_weather_status.count('Rain')
        if rain_count >= num_of_entries / 2:
            overall_rain = True


        # check for sunny today
        a = 1
        f = owm.daily_forecast('Vancouver,CAN')
        sunny_fc_list = []
        while a <= 23:
            sunny_fc_list.append(f.will_be_sunny_at('2016-10-20 %d:00:00+00'))

        # this is the new weather searching algorithm i need to check if it will work
        if sunny_fc_list.count(True) >= 1:
            if sunny_fc_list.count(False) >= ((sunny_fc_list.count(False) - sunny_fc_list.count(True)) / 2):
                overall_not_sunny = True
            else:
                # need to figure out what to do if it is roughly equally sunny and not sunny
                pass
        """
        f = owm.daily_forecast('Vancouver,CAN')
        f.will_be_*** for example f.will_be_rainy
        """





class CarbonGreet(object):
    def __init__(self, client_id, user_id, source):
        self.client_id = client_id
        self.user_id = user_id
        self.source = source


def audio_to_mem_str(client_id, user_id, source):
    text = r.speech_to_text(client_id, user_id, source)
    # Initializes the StringIO() module, which is used to write data to memory
    output = StringIO()
    # Takes the data contained in 'text' and writes it to memory
    output.write(text)
    # Retrieves the data stored in memory and prints it
    text=output.getvalue()
    output.close()
    return text
