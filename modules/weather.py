import pyowm
import os
import re
import speech_recognition as sr


class recognize_audio(object):

    def __init__(self, client_id, user_id, source):
        """
        Initializes the variables that are going to be used in STT translation
        """
        self.source = source
        self.client_id = client_id
        self.user_id = user_id

    def speech_to_text(self, client_id, user_id, source):
        r = sr.Recognizer()
        audio = r.listen(source)  # listens to the mic
        text = r.recognize_houndify(audio, self.client_id, self.user_id)
        return text

owm_api_key = 'ec64729a33987c7493bae7112e552ce5'
owm = pyowm.OWM(owm_api_key)

# This is the Canadian country code according to ISO 3166 standards
canada_cc = 'CAN'

client_id = 'IDvCAC3BVA7OT6wpF-8Ugw=='
user_id = 'UZ88zlplTdngMb4gvEJBnlp6VAIzrOZ_nQ7KgqV-b4nQtqr6QfZluankTTbpRLIwxU5FQSI95gh_eTNAngOv0g=='

with sr.Microphone() as source:
    r = recognize_audio(client_id, user_id, source)
    os.system("espeak 'For what city would you like to look up the weather for'")
    text = r.speech_to_text(client_id, user_id,source)
    os.system("espeak 'Did you say %s'" % text)
    text = r.speech_to_text(client_id,user_id, source)
    if re.search('yes', text):
        pass