import time
import os


class Module(object):
	
	def __init__(self):
		self.current_date=time.strftime('%B %d %Y')

	def execute(self):
		os.system("espeak 'Today is %s'" % self.current_date)

	def test(self):
		print('%s' % self.current_date)


