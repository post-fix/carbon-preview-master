
class recognize_audio():
    def __init__(self, client_id, user_id, source):
        """
        Initializes the variables that are going to be used in STT translation
        """
        source=source
        client_id=client_id
        user_id=user_id
        
    def speech_to_text(client_id, user_id, source):
        audio=r.listen(source)#listens to the mic
        text=r.recognize_houndify(audio, client_id, user_id)
        return text
    
    
    
class preprogrammed_response():
    def __init__(self, client_id, user_id, source):
        """
        Initializes response words, and mic recording
        """
        self.source=source
        self.client_id=client_id
        self.user_id=user_id
        self.response_words='good|great|amazing|awesome|brilliant|fantastic|well'
        self.response_words_list=self.response_words.split('|')#Creates a list of the preprogrammed response words
        
    def carbon_response(self, client_id, user_id, source):
        text=recognize_audio.speech_to_text(client_id, user_id, source)
        
        yes_match={}#Initializes empty, this will contain the index number and the word from response_words_list if a match is found 
        for index,word in enumerate(self.response_words_list):#Enumerate returns index of element and the element itself
            if re.search(word, text):#checks to see if the word is present in the translated text from user
                yes_match[index]=word#If the word is there, update yes_match{}
                break #A match was detected so no need to do anymore processing
        
        if not len(yes_match):#checks to see if yes_match is empty, if so indicates that no previously known response word is used
            if re.search('(?<=doing) (\w+)', text):#positive lookbehind assertion, if match is true (doing is present) will grab the word after it
                response_word_new=re.search('(?<=doing) (\w+)')
                response_word_new=response_word_new.group().strip(" ' ")
                os.system("espeak 'New response word detected adding to known list of words")
                os.system("espeak 'New response word is %s'" % response_word_new)
                self.response_words_list.append(response_word_new)
                pass #replace later
            else:
                os.system("espeak 'Response not valid, please try again'")
        else:
            previous match was detected, no need to do anything else
        
            
        """
        a=re.search("(?<=doing) (\w+)", phrase)
        This will match a single word after the word 'doing'
        The above is called a 'positive lookbehind assertion see the following link for more information:
            https://docs.python.org/2/library/re.html
            Search for 'lookbehind'
        """